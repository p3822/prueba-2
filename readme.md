# Prueba 2
## Para posición en remoto


endpoint para el registro y consulta de figuras


- implementación de backend con sprint boot
- base de datos en postgres
- pruebas unitarias de guardado

La solución corre en la siguiente url
```sh
localhost:8082
```
para poder correr la solución hay que correr el [script](https://gitlab.com/p3822/prueba-2/-/blob/master/src/main/resources/figuras.sql) en un postgres con version 13.4 en local

para conocer todos los endpoint disponibles se encuentrab en 

[endpoints](https://www.getpostman.com/collections/8dfb2ffde54c5f5183f1)



