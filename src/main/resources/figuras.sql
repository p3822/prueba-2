--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4 (Ubuntu 13.4-1.pgdg20.10+1)
-- Dumped by pg_dump version 13.4 (Ubuntu 13.4-1.pgdg20.10+1)

-- Started on 2021-12-03 14:17:53 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3083 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 201 (class 1259 OID 190909)
-- Name: figura; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.figura (
    id integer NOT NULL,
    base double precision,
    altura double precision,
    area double precision,
    diametro double precision,
    estatus boolean,
    tipo_id integer,
    fecha_alta date,
    fecha_baja date
);


ALTER TABLE public.figura OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 190907)
-- Name: figura_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.figura_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.figura_id_seq OWNER TO postgres;

--
-- TOC entry 3084 (class 0 OID 0)
-- Dependencies: 200
-- Name: figura_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.figura_id_seq OWNED BY public.figura.id;


--
-- TOC entry 203 (class 1259 OID 190917)
-- Name: tipo_figura; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_figura (
    id integer NOT NULL,
    tipo character varying(10) NOT NULL,
    estatus boolean NOT NULL,
    fecha_alta date,
    fecha_baja date
);


ALTER TABLE public.tipo_figura OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 190915)
-- Name: tipo_figura_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_figura_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_figura_id_seq OWNER TO postgres;

--
-- TOC entry 3085 (class 0 OID 0)
-- Dependencies: 202
-- Name: tipo_figura_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_figura_id_seq OWNED BY public.tipo_figura.id;


--
-- TOC entry 2937 (class 2604 OID 190912)
-- Name: figura id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.figura ALTER COLUMN id SET DEFAULT nextval('public.figura_id_seq'::regclass);


--
-- TOC entry 2938 (class 2604 OID 190920)
-- Name: tipo_figura id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_figura ALTER COLUMN id SET DEFAULT nextval('public.tipo_figura_id_seq'::regclass);


--
-- TOC entry 3075 (class 0 OID 190909)
-- Dependencies: 201
-- Data for Name: figura; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.figura (id, base, altura, area, diametro, estatus, tipo_id, fecha_alta, fecha_baja) FROM stdin;
\.


--
-- TOC entry 3077 (class 0 OID 190917)
-- Dependencies: 203
-- Data for Name: tipo_figura; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_figura (id, tipo, estatus, fecha_alta, fecha_baja) FROM stdin;
1	circulo	t	2021-03-12	\N
2	triangulo	t	2021-03-12	\N
3	cuadrado	t	2021-03-12	\N
\.


--
-- TOC entry 3086 (class 0 OID 0)
-- Dependencies: 200
-- Name: figura_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.figura_id_seq', 4, true);


--
-- TOC entry 3087 (class 0 OID 0)
-- Dependencies: 202
-- Name: tipo_figura_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_figura_id_seq', 1, true);


--
-- TOC entry 2940 (class 2606 OID 190914)
-- Name: figura figura_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.figura
    ADD CONSTRAINT figura_pk PRIMARY KEY (id);


--
-- TOC entry 2942 (class 2606 OID 190925)
-- Name: tipo_figura tipo_figura_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_figura
    ADD CONSTRAINT tipo_figura_pk PRIMARY KEY (id);


--
-- TOC entry 2943 (class 2606 OID 190929)
-- Name: figura figura_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.figura
    ADD CONSTRAINT figura_fk FOREIGN KEY (tipo_id) REFERENCES public.tipo_figura(id);


-- Completed on 2021-12-03 14:17:53 CST

--
-- PostgreSQL database dump complete
--

