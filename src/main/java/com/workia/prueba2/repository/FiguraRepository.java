package com.workia.prueba2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.workia.prueba2.modelo.Figura;

public interface FiguraRepository extends JpaRepository<Figura,Integer> {
    Page<Figura> findAllByEstatusIsTrue(Pageable page);
    Page<Figura> findAllByEstatusIsTrueAndTipoId_tipo(String tipo,Pageable page);
    Figura findByEstatusIsTrueAndId(Integer id);
}
