package com.workia.prueba2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workia.prueba2.modelo.TipoFigura;

public interface TipoFiguraRepository extends JpaRepository<TipoFigura, Integer> {
	TipoFigura findFirstByTipoAndEstatusIsTrue(String tipo);
	
}
