/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workia.prueba2.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j;

/**
 *
 * @author yair
 */
public class Circulo extends Figura {
	private static final long serialVersionUID = 1L;
    public Circulo(com.workia.prueba2.modelo.Figura figura) {
        this.diametro = figura.getDiametro();
        this.tipo = figura.getTipoId().getTipo();
        this.id =figura.getId();
   }
    
    @Override
    public Double getSuperficie() {
        return Math.sqrt( Math.PI * Math.pow((diametro/2), 2));
    }
}
