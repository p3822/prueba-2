/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workia.prueba2.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author yair
 */
@Setter @Getter @NoArgsConstructor
public  class Figura implements IGestionDatos, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Integer id;
	protected Double altura;
    protected Double base;
    protected Double diametro;
    protected String tipo;
	@Override
	public Double getSuperficie() {
		// TODO Auto-generated method stub
		return null;
	}
            
}
