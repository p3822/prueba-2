/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workia.prueba2.dto;

import lombok.Setter;

/**
 *
 * @author yair
 */

public class Triangulo extends Figura{
	private static final long serialVersionUID = 1L;
    public Triangulo(com.workia.prueba2.modelo.Figura figura) {
        this.base = figura.getBase();
        this.altura = figura.getAltura();
        this.tipo = figura.getTipoId().getTipo();
        this.id =figura.getId();
    }

    
    @Override
    public Double getSuperficie() {
        return (base * altura)/2;
    }

    
}
