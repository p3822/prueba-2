/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workia.prueba2.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author yair
 */

public class Cuadrado extends Figura{
	private static final long serialVersionUID = 1L;
    public Cuadrado(com.workia.prueba2.modelo.Figura figura) {
        this.base = figura.getBase();
        this.tipo = figura.getTipoId().getTipo();
        this.id =figura.getId();
    }

    @Override
    public Double getSuperficie() {
        return base* base;
    }
    
}
