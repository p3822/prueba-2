package com.workia.prueba2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.workia.prueba2.modelo.Figura;
import com.workia.prueba2.repository.FiguraRepository;
import com.workia.prueba2.utileria.Constantes;
import com.workia.prueba2.utileria.FiguraFactory;
@Service
public class FiguraService implements IFiguraService {
	@Autowired
	FiguraRepository figuraRepository;
        @Autowired
        FiguraFactory figuraFactory;

	@Override
	public com.workia.prueba2.dto.Figura deleteFigure(Integer idFigura) {
		Figura figura = findById(idFigura);
		figura.setEstatus(Constantes.ELIMINADO);
		return figuraFactory.getFigura(figuraRepository.save(figura));
	}

	@Override
	public Figura findById(Integer idFigura) {
		Figura figura =figuraRepository.findByEstatusIsTrueAndId(idFigura);
				if(figura== null) {
				     throw new ResponseStatusException(
					           HttpStatus.NOT_FOUND, "Registro no encontrado");
				}else {
					return figura;			
				}
	}

    @Override
    public com.workia.prueba2.dto.Figura findByIdDto(Integer Id) {
    return figuraFactory.getFigura(findById(Id));
    }

    @Override
    public Page<com.workia.prueba2.dto.Figura> getAllFiguras(Integer nPagina, Integer nElementos, String tipo) {
        Pageable ordenadoPorTipo = PageRequest.of(nPagina, nElementos, Sort.by("id").descending());
        return tipo.equals(Constantes.VACIO) ? figuraFactory.getFigura(figuraRepository.findAllByEstatusIsTrue(ordenadoPorTipo)):figuraFactory.getFigura(figuraRepository.findAllByEstatusIsTrueAndTipoId_tipo(tipo,ordenadoPorTipo));
    }
    
	@Override
	public com.workia.prueba2.dto.Figura saveFigure(com.workia.prueba2.dto.Figura figura) {
		return figuraFactory.getFigura(figuraRepository.save(figuraFactory.getFiguraFiltrada(figura)));
	}
	@Override
	public com.workia.prueba2.dto.Figura putFigure(com.workia.prueba2.dto.Figura figura) {
		findById(figura.getId());
		return figuraFactory.getFigura(figuraRepository.save(figuraFactory.getFiguraFiltrada(figura)));
		
	}

}
