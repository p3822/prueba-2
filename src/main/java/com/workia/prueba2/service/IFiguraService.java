package com.workia.prueba2.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.workia.prueba2.dto.Figura;
import com.workia.prueba2.modelo.TipoFigura;

public interface IFiguraService {
	Page<Figura> getAllFiguras(Integer nPagina, Integer nElementos, String tipo );
        Figura findByIdDto(Integer Id);
        com.workia.prueba2.modelo.Figura findById(Integer idFigura);
	Figura saveFigure(Figura figura);
	Figura putFigure(Figura figura);
	Figura deleteFigure(Integer idFigura);
}
