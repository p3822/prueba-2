package com.workia.prueba2.utileria;

import com.workia.prueba2.dto.Circulo;
import com.workia.prueba2.dto.Cuadrado;
import com.workia.prueba2.dto.Figura;
import com.workia.prueba2.dto.Triangulo;
import com.workia.prueba2.modelo.TipoFigura;
import com.workia.prueba2.repository.TipoFiguraRepository;

import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.persistence.annotations.TimeOfDay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
@Component
@Log
public class FiguraFactory {
	@Autowired
	TipoFiguraRepository tipoFiguraRepository;

	
        public  Page<Figura> getFigura(Page <com.workia.prueba2.modelo.Figura> page ){
            List<Figura> figurasTransformadas = new ArrayList<>();
            List<com.workia.prueba2.modelo.Figura> figuras = page.getContent();
            figuras.forEach((figura) -> {
                figurasTransformadas.add(getFigura(figura));
            });
            return new PageImpl<>(figurasTransformadas);
        }
    
    

	public   Figura getFigura( com.workia.prueba2.modelo.Figura figura){
                String tipo = figura.getTipoId().getTipo();
		if(tipo.equals(Constantes.CIRCULO)) {
                        return new Circulo(figura);
		}
		if(tipo.equals(Constantes.CUADRADO)) {
			return new Cuadrado(figura);
		}
		if(tipo.equals(Constantes.TRIANGULO)) {
			return new Triangulo(figura);
		}
		return null;
	}
        
        
        public  com.workia.prueba2.modelo.Figura getFiguraFiltrada(Figura figuraIn){
        	com.workia.prueba2.modelo.Figura figura = new com.workia.prueba2.modelo.Figura();
        	figura.setTipoId(getTipoFigura(figuraIn.getTipo()));
        	figura.setEstatus(Constantes.ACTIVO);
        	figura.setFechaAlta(new Date());
        	figura.setId(figuraIn.getId());
        	   log.info("........................." + figuraIn.getTipo());
            switch(figuraIn.getTipo()){
            
                case Constantes.CIRCULO:
                	figura.setAltura(null);
                	figura.setBase(null);
                	figura.setArea(figuraIn.getSuperficie());
                	figura.setDiametro(figuraIn.getDiametro());
                    if(figura.getDiametro() == null){
                        throw new ResponseStatusException(
					           HttpStatus.CONFLICT, "Error falta el diametro");
                    }
                    
                    break;
                case Constantes.CUADRADO:
                    figura.setAltura(null);
                    figura.setDiametro(null);
                    figura.setArea(getFigura(figura).getSuperficie());
                    figura.setBase(figuraIn.getBase());
                    if(figura.getBase() == null){
                        throw new ResponseStatusException(
					           HttpStatus.CONFLICT, "Error falta la base");
                    }
                    break;
                case Constantes.TRIANGULO:
                    figura.setDiametro(null);
                    figura.setArea(getFigura(figura).getSuperficie());
                    figura.setBase(figuraIn.getBase());
                    if(figura.getBase() == null){
                        throw new ResponseStatusException(
					           HttpStatus.CONFLICT, "Error falta la base");
                    }
                    figura.setAltura(figuraIn.getAltura());
                    if(figura.getAltura() == null){
                        throw new ResponseStatusException(
					           HttpStatus.CONFLICT, "Error falta la Altura");
                    }
                    break;
                default:
                    throw new ResponseStatusException(
					           HttpStatus.CONFLICT, "Error en transformado de figura");
                    
            }
            
            return figura;
	}
        
	TipoFigura getTipoFigura(String tipo) {
		TipoFigura tipoFigura = tipoFiguraRepository.findFirstByTipoAndEstatusIsTrue(tipo);
		if(tipoFigura == null) {
			throw new ResponseStatusException(
			           HttpStatus.CONFLICT, "No se encontro el tipo de figura");	
		}
		return tipoFigura;
		
		
	}


	
}
