package com.workia.prueba2.utileria;

public class Constantes {
	public final static String CIRCULO ="circulo";
	public final static String CUADRADO ="cuadrado";
	public final static String TRIANGULO ="triangulo";
	
	public final static Boolean ELIMINADO =false;
	public final static Boolean ACTIVO =true;
	
	public final static String VACIO ="------";
	
}
