package com.workia.prueba2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workia.prueba2.dto.Figura;
import com.workia.prueba2.service.FiguraService;
import com.workia.prueba2.utileria.Constantes;

@RestController
@RequestMapping("/figura")
public class FiguraController {
@Autowired
FiguraService figuraService;

	@GetMapping(value = "/", params = {"nPagina", "nElementos", "tipo"})
	public Page<com.workia.prueba2.dto.Figura> getTodasLasFiguras(@RequestParam Integer nPagina,
			@RequestParam Integer nElementos,
			@RequestParam(required = false, defaultValue = Constantes.VACIO) String tipo ){
		return figuraService.getAllFiguras(nPagina, nElementos, tipo);
	}
	
	@GetMapping(value = "/{id}")
	public com.workia.prueba2.dto.Figura getTodasLasFiguras(@PathVariable Integer id){
		return figuraService.findByIdDto(id);
	}
	
	
	@PostMapping("/")
	public com.workia.prueba2.dto.Figura guardarFiguras(@RequestBody Figura figura){
		return figuraService.saveFigure(figura);
	}
	
	@PutMapping("/{id}")
	public com.workia.prueba2.dto.Figura editarFiguras(@PathVariable Integer id, @RequestBody Figura figura){
		figura.setId(id);
		return figuraService.putFigure(figura);
	}
	@DeleteMapping("/{id}")
	public com.workia.prueba2.dto.Figura eliminarFiguras(@PathVariable Integer id){
		return figuraService.deleteFigure(id);
	}
}
