package com.workia.prueba2.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.workia.prueba2.modelo.Figura;
import com.workia.prueba2.modelo.TipoFigura;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FiguraRepositoryTest {
		@Autowired
	    private TestEntityManager entityManager;
		@Autowired
		private TipoFiguraRepository tipoFiguraRepository;
		@Autowired
		private FiguraRepository figuraRepository;
		
		
		@Test
		public void test() {
			TipoFigura tipoFigura = new TipoFigura();
			tipoFigura.setEstatus(true);
			tipoFigura.setFechaAlta(new Date());
			tipoFigura.setTipo("circulo");
			tipoFiguraRepository.save(tipoFigura);
			tipoFigura =  tipoFiguraRepository.findById(1).get();

			Figura figura = new Figura();
			figura.setDiametro(3.0);
			figura.setTipoId(tipoFigura);
			figura.setEstatus(false);
			figuraRepository.save(figura);
			List<Figura> listaFigura = figuraRepository.findAll();
			assertThat(listaFigura.size()).isGreaterThan(0);
		}
	}


